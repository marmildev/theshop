﻿namespace TheShop
{
    interface ISupplier
    {
        bool ArticleInInventory(int id);
        Article GetArticle(int id);        
    }
}
