﻿using System;
using System.Collections.Generic;

namespace TheShop
{
	public class ShopService
	{
		private DatabaseDriver DatabaseDriver;
		private Logger logger;
        
        private List<ISupplier> _suppliers;
	    private List<Article> _articles;

		public ShopService()
		{
			DatabaseDriver = new DatabaseDriver();
			logger = new Logger();
            
		    _suppliers = new List<ISupplier>();
            _suppliers.Add(new Supplier1());
		    _suppliers.Add(new Supplier2());
		    _suppliers.Add(new Supplier3());

            _articles = new List<Article>();		    
        }
        
        /// <summary>
        /// Order article from list of suppliers
        /// </summary>
        /// <param name="id"></param>
        /// <param name="maxExpectedPrice"></param>
        /// <returns></returns>
	    public Article Order(int id, int maxExpectedPrice)
	    {
            if(id <= 0)
                throw new ArgumentOutOfRangeException("id");

            if(maxExpectedPrice <= 0)
                throw new ArgumentOutOfRangeException("maxExpectedPrice");

            //get articles from all suppliers            
            _articles.Clear();
	        foreach (var supplier in _suppliers)
	        {
	            if(supplier.ArticleInInventory(id))
	                _articles.Add(supplier.GetArticle(id));
            }

            //sort article list by price - ascending
            SortArticleList(_articles);                          

            // find first which price is under maxExpectedPrice
            var article = _articles.Find(art => art.ArticlePrice <= maxExpectedPrice);
	        if (article != null)
	        {
	            return article;
            }	            
	        else
	        {
	            logger.Info("Article with ID: " + id + " and price lower then " + maxExpectedPrice + " is not found! Min price is: " + _articles[0].ArticlePrice);
	            return null;
	        }
	    }

        /// <summary>
        /// Slling article
        /// </summary>
        /// <param name="article"></param>
        /// <param name="buyerId"></param>
	    public void Sell(Article article, int buyerId)
	    {
	        if (article == null)	        	            
                throw new NullReferenceException("Could not sell this article! Given article is null!");                
	        
            if(buyerId <= 0)
                throw new ArgumentOutOfRangeException("buyerId");

	        logger.Debug("Trying to sell article with id = " + article.ID);

	        article.IsSold = true;
	        article.SoldDate = DateTime.Now;
	        article.BuyerUserId = buyerId;

	        try
	        {
	            DatabaseDriver.Save(article);
	            logger.Info("Article with id = " + article.ID + " is sold.");
	        }	        
	        catch (Exception ex)
	        {
                logger.Error(ex.Message);
	        }
        }
        
        /// <summary>
        /// Get article by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		public Article GetById(int id)
		{
            if(id <= 0)
                throw new ArgumentOutOfRangeException("id");

            var article = DatabaseDriver.GetById(id);
		    if (article != null)
		        return article;

		    return null;
		}


        /// <summary>
        /// Sorting list by price
        /// </summary>
        /// <param name="articles"></param>
	    private void SortArticleList(List<Article> articles)
	    {
	        if (articles != null && articles.Count > 0)
	        {
	            // sort by price
	            articles.Sort
	            (
	                (article1, article2) =>
	                {
	                    if (article1.ArticlePrice < article2.ArticlePrice)
	                        return -1;
	                    if (article1.ArticlePrice > article2.ArticlePrice)
	                        return 1;

	                    return 0;
	                }
	            );
            }            
        }

        #region Unit Tests service methods
        /// <summary>
        /// Find article with lowest price by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
	    public int FindWithLowestPrice(int id)
	    {
	        var articles = new List<Article>();
	        foreach (var supplier in _suppliers)
	        {
	            if (supplier.ArticleInInventory(id))
	                articles.Add(supplier.GetArticle(id));
	        }

	        SortArticleList(articles);

	        return articles[0].ArticlePrice;
	    }

        #endregion
    }		    
}
