﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TheShop
{
    //in memory implementation
    public class DatabaseDriver
    {
        private List<Article> _articles = new List<Article>();

        public Article GetById(int id)
        {
            try
            {
                return _articles.Single(x => x.ID == id);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void Save(Article article)
        {
            _articles.Add(article);
        }
    }
}
