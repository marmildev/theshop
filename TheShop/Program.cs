﻿using System;
using System.Runtime.CompilerServices;

namespace TheShop
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var shopService = new ShopService();
            Logger logger = new Logger(); 

			try
			{
				//order and sell
			    var article1 = shopService.Order(1, 458);
                shopService.Sell(article1, 10);

                var article2 = shopService.Order(1, 20);			    
                shopService.Sell(article2, 10);

            }
			catch (Exception ex)
			{
			    logger.Error(ex.Message);
            }
            
		    FindArticle(shopService, logger, 1);
		    FindArticle(shopService, logger, 12);

            Console.ReadKey();
		}

        /// <summary>
        /// Internal service method for searching some specific article by id
        /// </summary>
        /// <param name="shopService"></param>
        /// <param name="id"></param>
	    private static void FindArticle(ShopService shopService, Logger logger, int id)
	    {
	        try
	        {
	            //print article on console				
	            var article = shopService.GetById(id);
	            if (article != null)
	            {
                    logger.Info("Found article with ID: " + article.ID);	                
                }	                
                else
                {
                    logger.Info("Article with ID: " + id + " not found!");                    
                }
	        }
	        catch (Exception ex)
	        {
                logger.Error("Article not found: " + ex);	            
	        }
        }
	}
}