﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheShop;

namespace TheShopTests
{
    [TestClass]
    public class ShopServiceTest
    {
        #region Order method test cases

        /// <summary>
        /// case when id and maxExpectedPrice are valid 
        /// </summary>
        [TestMethod]
        public void Order_WithValidMaxExpectedPrice()
        {
            var id = 1;
            var shopService = new ShopService();
            var maxExpectedPrice = shopService.FindWithLowestPrice(id);

            var article = shopService.Order(id, maxExpectedPrice);

            Assert.IsNotNull(article);
        }

        /// <summary>
        /// case when maxExpectedPrice is less then zero 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Order_WhenMaxExpectedPriceIsLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            var id = 1;
            var shopService = new ShopService();
            var maxExpectedPrice = -5;

            var article = shopService.Order(id, maxExpectedPrice);
        }

        /// <summary>
        /// case when id is less then zero 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Order_WhenIdIsLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            var id = -11;
            var shopService = new ShopService();
            var maxExpectedPrice = shopService.FindWithLowestPrice(id); ;

            var article = shopService.Order(id, maxExpectedPrice);
        }

        #endregion

        #region Sell method test cases

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void Sell_ArticleIsNull_ShouldThrowNullReferenceException()
        {
            var buyerId = 1;
            var shopService = new ShopService();
            shopService.Sell(null, buyerId);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Sell_BuyerIdLessThenZero_ShouldThrowArgumentOutOfRangeException()
        {
            var buyerId = -1;
            var shopService = new ShopService();
            var article = new Article()
            {
                ID = 1,
                Name_of_article = "Test article",
                ArticlePrice = 459
            }; 

            shopService.Sell(article, buyerId);
        }

        #endregion

        #region Get/Display method test cases

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Get_IdLessThenZero_ShouldThrowArgumentOutOfRangeException()
        {
            var shopService = new ShopService();
            shopService.GetById(-11);
        }

        #endregion
    }
}
